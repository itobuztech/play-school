'use strict';


if (document.querySelector('#birthday-scene')) {
  gsap.to("#scene_1_sp", {
    opacity: 0,
    duration: 1,
    delay: 1,
    repeat: -1,
    ease: "power4.inOut",
    yoyo:true
  });
  gsap.to("#scene_1_sp1", {
    opacity: 0,
    duration: 1,
    delay: 1.2,
    repeat: -1,
    ease: "power4.inOut",
    yoyo:true
  });
  gsap.to("#scene_1_sp2", {
    opacity: 0,
    duration: 1,
    delay: 1.6,
    repeat: -1,
    ease: "power4.inOut",
    yoyo:true
  });
  gsap.to("#scene_1_h", {
    opacity: 0,
    duration: 1,
    delay: 1.5,
    repeat: -1,
    ease: "power4.inOut",
    yoyo:true
  });
  gsap.to("#scene_1_a", {
    opacity: 0,
    duration: 1,
    delay: 1.6,
    repeat: -1,
    ease: "power4.inOut",
    yoyo:true
  });
  gsap.to("#scene_1_p", {
    opacity: 0,
    duration: 1,
    delay: 1.7,
    repeat: -1,
    ease: "power4.inOut",
    yoyo:true
  });

  gsap.to("#scene_1_p1", {
    opacity: 0,
    duration: 1,
    delay: 1.8,
    repeat: -1,
    ease: "power4.inOut",
    yoyo:true
  });
  gsap.to("#scene_1_y", {
    opacity: 0,
    duration: 1,
    delay: 1.9,
    repeat: -1,
    ease: "power4.inOut",
    yoyo:true
  });
  gsap.to("#scene_1_b", {
    opacity: 0,
    duration: 1,
    delay: 1.2,
    repeat: -1,
    ease: "power4.inOut",
    yoyo:true
  });
  gsap.to("#scene_1_i", {
    opacity: 0,
    duration: 1,
    delay: 1.4,
    repeat: -1,
    ease: "power4.inOut",
    yoyo:true
  });
  gsap.to("#scene_1_r", {
    opacity: 0,
    duration: 1,
    delay: 1.6,
    repeat: -1,
    ease: "power4.inOut",
    yoyo:true
  });
  gsap.to("#scene_1_t", {
    opacity: 0,
    duration: 1,
    delay: 1.7,
    repeat: -1,
    ease: "power4.inOut",
    yoyo:true
  });
  gsap.to("#scene_1_hgreen", {
    opacity: 0,
    duration: 1,
    delay: 1.8,
    repeat: -1,
    ease: "power4.inOut",
    yoyo:true
  });

  gsap.to("#scene_1_cupcake", {
    transformOrigin: "50% 20%",
    rotation: 2,
    repeat: -1,
    duration: 2,
    yoyo: true
  });
  gsap.to("#scene_1_candlecake", {
    transformOrigin: "50% 20%",
    rotation: -2,
    repeat: -1,
    duration: 2,
    yoyo: true
  });
  gsap.to("#scene_1_cap", {
    x: -10,
    y: -10,
    repeat: -1,
    duration: 4,
    yoyo: true
  });
  gsap.to("#scene_1_gbaloon", {
    opacity: 0,
    y: -100,
    repeat: -1,
    duration: 2,
    delay: 1
  });
  gsap.to("#scene_1_bbaloon", {
    opacity: 0,
    y: -200,
    repeat: -1,
    duration: 3,
    delay: 0.5
  });
  gsap.set("#scene_1_partybg", {y: 0});
  gsap.from("#scene_1_partybg", 2.2, {
    opacity: 0,
    y: 100,
    ease: "Power4.easeInOut"
  });
  gsap.from("#scene_1_phz", {
    opacity: 0,
    y: -10,
    delay: 1.5,
    ease: "Bounce.easeInOut"
  });
  gsap.from("#scene_1_ahz", {
    opacity: 0,
    y: -10,
    delay: 1.7,
    ease: "Bounce.easeInOut"
  });
  gsap.from("#scene_1_rhz", {
    opacity: 0,
    y: -10,
    delay: 1.9,
    ease: "Bounce.easeInOut"
  });
  gsap.from("#scene_1_thz", {
    opacity: 0,
    y: -10,
    delay: 2,
    ease: "Bounce.easeInOut"
  });
  gsap.from("#scene_1_yhz", {
    opacity: 0,
    y: -10,
    delay: 2.2,
    ease: "Bounce.easeInOut"
  });
  gsap.from("#scene_1_tshz", {
    opacity: 0,
    y: -10,
    delay: 2.5,
    ease: "Bounce.easeInOut"
  });
  gsap.from("#scene_1_ihz", {
    opacity: 0,
    y: -10,
    delay: 2.7,
    ease: "Bounce.easeInOut"
  });
  gsap.from("#scene_1_mhz", {
    opacity: 0,
    y: -10,
    delay: 2.9,
    ease: "Bounce.easeInOut"
  });
  gsap.from("#scene_1_ehz", {
    opacity: 0,
    y: -10,
    delay: 3,
    ease: "Bounce.easeInOut"
  });
  gsap.to("#scene_1_yl", {
    skewY: 10,
    repeat: -1,
    duration: 3,
    ease: "Power3.easeInOut",
    yoyo: true
  });
  gsap.to("#scene_1_spr1", {
    opacity: 0,
    repeat: -1,
    delay: 1.3,
    ease: "Power4.ease",
    yoyo: true
  });
  gsap.to("#scene_1_spr2", {
    opacity: 0,
    repeat: -1,
    delay: 1.5,
    ease: "Power4.ease",
    yoyo: true
  });
  gsap.to("#scene_1_spr3", {
    opacity: 0,
    repeat: -1,
    delay: 1.7,
    ease: "Power4.ease",
    yoyo: true
  });
  gsap.to("#scene_1_spr4", {
    opacity: 0,
    repeat: -1,
    delay: 1.9,
    ease: "Power4.ease",
    yoyo: true
  });
  gsap.to("#scene_1_spl1", {
    opacity: 0,
    repeat: -1,
    delay: 1.3,
    ease: "Power4.ease",
    yoyo: true
  });
  gsap.to("#scene_1_spl2", {
    opacity: 0,
    repeat: -1,
    delay: 1.5,
    ease: "Power4.ease",
    yoyo: true
  });
  gsap.to("#scene_1_spl3", {
    opacity: 0,
    repeat: -1,
    delay: 1.7,
    ease: "Power4.ease",
    yoyo: true
  });
  gsap.to("#scene_1_spl4", {
    opacity: 0,
    repeat: -1,
    delay: 1.9,
    ease: "Power4.ease",
    yoyo: true
  });
  gsap.to("#scene_1_gspring", {
    y: -10,
    repeat: -1,
    duration: 1.5,
    ease: "Power4.ease",
    yoyo: true
  });
  gsap.to("#scene_1_bspring", {
    y: 10,
    repeat: -1,
    duration: 1.5,
    ease: "Power4.ease",
    yoyo: true
  });
}
    
  
    


const button = document.querySelector(".controller");

const audio = document.querySelector("audio");

button.addEventListener("click", () => {
  if (audio.paused) {
    audio.volume = 0.2;
    audio.play();
    button.classList.remove('play');
    button.classList.add('pause');
    
  } else {
    audio.pause();
    button.classList.remove('pause');
    button.classList.add('play');
  }
  
});
