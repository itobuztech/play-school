'use strict';

if (document.querySelector('#scene-1')) {
  gsap.to("#scene_1_circle", {
      rotation: 360,
      duration: 100,
      repeat: -1,
      transformOrigin: "50% 50%",
      ease: "none",
      yoyo:true
  });

  gsap.to("#scene_1_cloud", {
      x: '100%',
      duration: 150,
      repeat: -1,
      ease: "none"
  });
  gsap.to("#scene_1_cloud2", {
      x: '100%',
      duration: 150,
      repeat: -1,
      ease: "none"
  });
  gsap.set("#scene_1_text_1", {
      y: 856
  })
  gsap.set("#scene_1_text_2", {
      y: 652
  })
  gsap.set("#scene_1_text_3", {
      y: 429
  })

  gsap.to("#scene_1_text_1", {
      y: 0,
      duration: 3,
      ease: "elastic.inOut(1, 0.3)",
      delay: .5
  });
  gsap.to("#scene_1_text_2", {
      y: 0,
      duration: 3,
      ease: "elastic.inOut(1, 0.3)",
      delay: .7
  });
  gsap.to("#scene_1_text_3", {
      y: 0,
      duration: 3,
      ease: "elastic.inOut(1, 0.3)",
      delay: 1
  });
  gsap.to("#scene_1_rocket", {
    scrollTrigger: {
      trigger: "#scene-1",
      start: "top top",
      end: "bottom 20px",
      scrub: true
    },
    opacity: 0,
    y: -300,
    x: 300, 
    duration: 3
  });
  gsap.to("#scene_1_globe", {
    scrollTrigger: {
      trigger: "#scene-1",
      start: "top top",
      end: "bottom 20px",
      scrub: 1
    },
    opacity: 0,
    x: -100, 
    transformOrigin: "50% 50%",
    rotation: 360,
    duration: 5
  });
  gsap.to("#scene_1_redstar", {
    opacity: 0,
    repeat: -1,
    duration: 1,
    yoyo: true
  });
  gsap.to("#scene_1_redstarsmall", {
    opacity: 0,
    repeat: -1,
    duration: 1.2,
    yoyo: true
  });
  gsap.to("#scene_1_violetstar", {
    opacity: 0,
    repeat: -1,
    duration: 1.1,
    yoyo: true
  });
  gsap.to("#scene_1_circle_1", {
    y: 10,
    repeat: -1,
    duration: 1,
    yoyo: true
  });
  gsap.to("#scene_1_circle_2", {
    x: 10,
    repeat: -1,
    duration: 1.3,
    yoyo: true
  });
  gsap.to("#scene_1_circle_3", {
    x: 10,
    repeat: -1,
    duration: 1.1,
    yoyo: true
  });
  gsap.to("#scene_1_circle_4", {
    x: 10,
    repeat: -1,
    duration: 1.3,
    yoyo: true
  });
  gsap.to("#scene_1_circle_5", {
    x: 10,
    y: 10,
    repeat: -1,
    duration: 1.2,
    yoyo: true
  });
  gsap.to("#scene_1_circle_7", {
    opacity: 0,
    repeat: -1,
    duration: 1.2,
    yoyo: true
  });
}

if (document.querySelector('#scene-3')) {
  gsap.to("#scene_3_star_1", {
    opacity: 0,
    repeat: -1,
    duration: 1,
    yoyo: true
  });
  gsap.to("#scene_3_star_2", {
    opacity: 0,
    repeat: -1,
    duration: 1.2,
    yoyo: true
  });
  gsap.to("#scene_3_star_3", {
    opacity: 0,
    repeat: -1,
    duration: 1.4,
    yoyo: true
  });
  gsap.to("#scene_3_star_4", {
    opacity: 0.3,
    repeat: -1,
    duration: 1.1,
    yoyo: true
  });
  gsap.to("#scene_3_star_5", {
    opacity: 0,
    repeat: -1,
    duration: 1.2,
    yoyo: true
  });
  gsap.to("#scene_3_globe", {
    y: 10,
    x: 10,
    repeat: -1,
    duration: 1.2,
    yoyo: true
  });
}

if (document.querySelector('#scene-4')) {
  gsap.to("#scene_4_star_1", {
    opacity: 0,
    repeat: -1,
    duration: 1,
    yoyo: true
  });
  gsap.to("#scene_4_star_2", {
    opacity: 0,
    repeat: -1,
    duration: 1.2,
    yoyo: true
  });
  gsap.to("#scene_4_star_3", {
    opacity: 0,
    repeat: -1,
    duration: 1.4,
    yoyo: true
  });
  gsap.to("#scene_4_star", {
    opacity: 0.3,
    repeat: -1,
    duration: 1.1,
    yoyo: true
  });
  gsap.to("#scene_4_earth", {
    y: 10,
    x: 10,
    repeat: -1,
    duration: 1.2,
    yoyo: true
  });
  gsap.to("#scene_4_galaxy", {
    transformOrigin: "50% 50%",
    rotation: 360,
    repeat: -1,
    duration: 7,
    yoyo: true
  });
  gsap.to("#scene_4_tube", {
    transformOrigin: "0 50%",
    rotation: 40,
    repeat: -1,
    duration: 4,
    yoyo: true
  });
}

if (document.querySelector('#scene-5')) {
  gsap.to("#scene_5_bulb", {
    opacity: 0.4,
    repeat: -1,
    duration: 2,
    yoyo: true
  });
  gsap.to("#scene_5_tube", {
    transformOrigin: "0 0",
    rotation: 5,
    repeat: -1,
    duration: 2,
    yoyo: true
  });
  gsap.to("#scene_5_draw", {
    x: 10,
    repeat: -1,
    duration: 2,
    yoyo: true
  });
  gsap.to("#scene_5_globe", {
    y: 10,
    repeat: -1,
    duration: 2,
    yoyo: true
  });
  gsap.to("#scene_5_girl", {
    scrollTrigger: {
      trigger: "#scene_5_girl",
      start: "top top",
      end: "bottom bottom",
      scrub: 1
    },
    y: 40,
    duration: 2,
    yoyo: true
  });
}

gsap.to(".home-section2 .deepblue-globe", {
  rotation: 360,
  duration: 80,
  repeat: -1,
  transformOrigin: "50% 50%"
});
gsap.to(".home-section2 .girl", {
  y: 30,
  repeat: -1,
  duration: 1.7,
  ease: "power2.inOut",
  yoyo: true
});
gsap.to(".home-section4 .boy-logo", {
  rotation: 360,
  duration: 80,
  repeat: -1,
  transformOrigin: "50% 50%"
});

gsap.to(".home-section10__form .earth", {
  y: 10,
  duration: 1.5,
  repeat: -1,
  ease: "power4.Out",
  yoyo: true
});
gsap.to(".home-section10__form .love-sym", {
  skewX: 10,
  duration: 1,
  repeat: -1,
  ease: "power4.Out",
  yoyo: true
});
gsap.to(".home-section5 .news-leaf", {
  skewY: 10,
  duration: 3,
  repeat: -1,
  ease: "power4.Out",
  yoyo: true
});
gsap.to(".home-section9 .news-leaf", {
  skewY: 10,
  duration: 3,
  repeat: -1,
  ease: "power4.Out",
  yoyo: true
});
gsap.to(".home-section8 .leaf", {
  skewY: 10,
  duration: 3,
  repeat: -1,
  ease: "power4.Out",
  yoyo: true
});
gsap.to(".home-section8 .book", {
  scrollTrigger: {
    trigger: ".home-section7",
    start: "50% top",
    end: "bottom 20px",
    scrub: true
  },
  y: -50,
  opacity: 0,
  duration: 5
});
gsap.to(".home-section9 .ring", {
  y: 100,
  x: 100,
  opacity: 0,
  duration: 2,
  repeat: -1,
  ease: "power4.Out",
  yoyo: true
});
//=== section wise appearance ==== //
gsap.fromTo(".home-section2", 
{
  opacity: 0, 
  y: -50
}, 
{
  scrollTrigger: {
    trigger: ".home-section1",
    start: "80px top",
    end: "bottom bottom"
  },
  y: 0,
  opacity: 1, 
  duration: 1
});
gsap.fromTo(".home-section3 .section-title", 
{
  opacity: 0, 
  y: 50
}, 
{
  scrollTrigger: {
    trigger: ".home-section2",
    start: "top top",
    end: "bottom bottom"
  },
  y: 0,
  opacity: 1, 
  duration: 1
});
// gsap.fromTo(".home-section3 .section-img", 
// {
//   opacity: 0, 
//   x: 50
// }, 
// {
//   scrollTrigger: {
//     trigger: ".home-section2",
//     start: "top top",
//     end: "bottom bottom"
//   },
//   x: 0,
//   opacity: 1, 
//   duration: 1
// });
gsap.fromTo(".home-section4 .section-img", 
{
  opacity: 0, 
  y: 50
}, 
{
  scrollTrigger: {
    trigger: ".home-section3",
    start: "30px top",
    end: "bottom bottom"
  },
  y: 0,
  opacity: 1, 
  duration: 1
});


//==================== SWIPER JS ===========//

//home section 2 -swiper slider
var swiper = new Swiper('.home-section2 .swiper-container', {
  autoplay: {
    delay: 5000
  },
  navigation: {
    nextEl:'.swiper-button-next',
    prevEl:'.swiper-button-prev',
  },
});
//home section 7 -swiper slider (image gallery)
var swiper1 = new Swiper('.home-section7 .swiper-container', {
  autoplay: {
    delay: 1000
  },
  navigation: {
    nextEl:'.swiper-button-next',
    prevEl:'.swiper-button-prev',
  },
  loop: true,
  slidesPerView: 1,
  spaceBetween: 30,
  breakpoints: {
    768: {
      slidesPerView: 2,
    },
    1200: {
      slidesPerView: 4,
    }
  }
});
//home section 8 -swiper slider (testimonial)
var swiper2 = new Swiper('.home-section8 .swiper-container', {
  autoplay: {
    delay: 4000
  },
  navigation: {
    nextEl:'.swiper-button-next',
    prevEl:'.swiper-button-prev',
  },
});
//home section 9 -swiper slider (latest news)
var swiper3 = new Swiper('.home-section9 .swiper-container', {
  autoplay: {
    delay: 3000
  },
  navigation: {
    nextEl:'.swiper-button-next',
    prevEl:'.swiper-button-prev',
  },
  loop: true,
  slidesPerView: 1,
  spaceBetween: 30,
  breakpoints: {
    992: {
      slidesPerView: 2,
    },
  }
});
//home section 10 - form validation
if(document.querySelector('#enquiry-form')) {
  $('#enquiry-form').validate();
}
