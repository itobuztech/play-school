'use strict';

$(document).ready(function() {
  $(".nav-toggle").on('click', function() {
    $(this).toggleClass("isClick");
    $(this).siblings('ul').toggleClass("isOpen");
    $('body').toggleClass('noOverflow');
  });

  gsap.to(".main-footer .red-star", {
    opacity: 0,
    repeat: -1,
    duration: 1.8,
    ease: "power4.inOut",
    yoyo: true
  });

  //page smooth scroll
  
}); 

